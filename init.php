<?php defined('SYSPATH') OR die('No direct access allowed.');

$include_path = ini_get( 'include_path' );
$vendor_path  = MODPATH . 'flickr/vendor/';

if( ini_set( 'include_path', $include_path . PATH_SEPARATOR . $vendor_path ) === FALSE )
{
	throw new Kohana_Exception( "Whoa! I'm not allowed to set the include path!" );
	exit( );
}

require_once Kohana::find_file( 'vendor/Phlickr', 'Api' );
require_once Kohana::find_file( 'vendor/Phlickr', 'PhotosetList' );


Route::set('flickr', 'flickr(/<action>(/<id>(/<page>)))', array('id' => '\d+', 'page' => '\d+'))
	->defaults(array(
		'controller' => 'flickr',
		'action'     => 'index',
	));

