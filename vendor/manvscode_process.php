<?php
@session_start( );

$include_path = ini_get( 'include_path' );
if( ini_set( 'include_path', $include_path . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . '/application/vendor' ) === FALSE )
{
	throw new Kohana_Exception( "Whoa! I'm not allowed to set the include path!" );
	exit( );
}

define('API_KEY', '8a64640c4b402b3b8a25be59c279950a' );
define('API_SECRET', '52421e3907a2245c' );
include_once 'Phlickr/Api.php';
$api = new Phlickr_Api(API_KEY, API_SECRET);


$frob = $_SESSION['app_frob'];
echo "Frob: $frob<br />\n";
// After they've granted permission, convert the frob to a token.
$token = $api->setAuthTokenFromFrob( $frob );

// Print out the token.
echo "Auth token: $token<br />\n";

// Optionally, create a config file.
    echo 'Filename: ';
    $filename = 'flickr.cfg';
    echo "Saving settings to '$filename'\n";
    $api->saveAs($filename);
    echo "Use this with Phlickr_Api::createFrom() to create an object.<br />\n";

exit(0);

?>
