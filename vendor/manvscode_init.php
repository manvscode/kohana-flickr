<?php
@session_start( );

$include_path = ini_get( 'include_path' );
if( ini_set( 'include_path', $include_path . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . '/application/vendor' ) === FALSE )
{
	throw new Kohana_Exception( "Whoa! I'm not allowed to set the include path!" );
	exit( );
}

define('API_KEY', '8a64640c4b402b3b8a25be59c279950a' );
define('API_SECRET', '52421e3907a2245c' );
include_once 'Phlickr/Api.php';
$api = new Phlickr_Api(API_KEY, API_SECRET);

// Authentication is no longer done with an email/password. the first step
// is requesting a frob and then building a Flickr URL to log into.
$frob = $api->requestFrob();
// request read and write permissions (write implies read)
$url = $api->buildAuthUrl('read', $frob);
echo "frob: $frob<br />\n";
// you'll need open the link and log into you Flickr account to grant
// permissions to this API key.
echo "url: $url<br />\n";


$_SESSION[ 'app_frob' ] = $frob;
