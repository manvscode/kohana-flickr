<?php 

include_once 'Phlickr/Api.php';
$api = Phlickr_Api::createFrom('manvscode_flickr.cfg');

// check if the authentication token is valid.
print $api->isAuthValid();

// execute a flickr echo test method.
$response = $api->executeMethod('flickr.test.login', array('foo'=>'bar'));
//$response = $api->executeMethod('flickr.test.echo', array('foo'=>'bar'));
print $response;
