/*
 * Copyright 2007-2009 by Joe Marrero <manvscode@gmail.com>
 *
 */

jQuery.fn.flickrSlideshow = function( flickr_photoset_id, opts ) {
	var self = this;

	self.plan              = [];
	self.image_size        = opts.image_size == undefined ? 't' : opts.image_size;
	self.time              = opts.time == undefined ? 2 : opts.time;
	self.crossSlideOptions = {
		sleep: opts.sleep == undefined ? 1 : opts.sleep,
		fade:  opts.fade == undefined ? 1 : opts.fade
	};
	
	$.getJSON( '/flickr/ajax_photos/' + flickr_photoset_id, function(json) {
		var froms = [ '100% 80% 1x', 'top left', 'top right', '100% 80% 1.5x', '100% 50%' ];
		var tos = [ '100% 0% 1.7x', 'bottom right 1.5x', '80% 0% 1.1x', '30% 50% 1.5x' ];
		
		$.each( json, function(i) {
				// this is current element
				self.plan.push( {
					src: this.sizes[self.image_size].source,
					from: froms[ Math.floor(Math.random() * froms.length) ],
					to:  tos[ Math.floor(Math.random() * tos.length) ],
					//href: this.url,
					time: self.time
				});
			}
		);
		self.crossSlide( self.crossSlideOptions, self.plan );
	});

	return self;
};


