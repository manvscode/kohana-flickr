function FlickrSlideShow(id, flickr_photoset_id, flickr_image_size, update_interval) {
	this.id                = id;
	this.image_elem        = $('#' + id);
	this.update_interval   = update_interval == undefined ? 10 : update_interval;
	this.images            = [];
	this.urls              = [];
	this.image_size        = flickr_image_size == undefined ? 2 : flickr_image_size;
	this.preload_window    = 1;
	this.set_parent_anchor = false;

	this.image_elem.fadeOut(0);
	
	var selfObject = this;
	setInterval(function(){ selfObject.update(); }, this.update_interval * 1000);

	$.getJSON( '/flickr/ajax_photos/' + flickr_photoset_id, function(json) {
			$.each( json, function(i) {
					// this is current element
					selfObject.images.push( this.images[selfObject.image_size] );
					selfObject.urls.push( this.url );
				}
			);
			
			// preload some of them at the beginning...
			selfObject.preload( selfObject.images.slice( 0, 1 ) );	

			// start the slideshow.
			selfObject.image_elem.text('');
			if( selfObject.set_parent_anchor ) selfObject.image_elem.parent().attr('href', json[0].url)
			selfObject.image_elem.attr('src', json[0].images[selfObject.image_size] );
			selfObject.image_elem.fadeIn( 900 );
		}
	);
};

FlickrSlideShow.prototype.update = function() {
	for( var i = 0; i < this.images.length; i++ )
	{
		if( this.image_elem.attr('src') == this.images[ i ] )
		{
			if( ++i == this.images.length )
			{
				i = 0;
			}
		
			var selfObject = this;	
			this.image_elem.fadeOut( 800, function() {
					if( selfObject.set_parent_anchor ) selfObject.image_elem.parent().attr('href', selfObject.urls[ i ]);
					selfObject.image_elem.attr('src', selfObject.images[ i ] );
					selfObject.image_elem.fadeIn( 800 );
				}
			);	


			// preload some of them at the beginning...
			selfObject.preload( this.images.slice( i, i + this.preload_window ) );	
			break;
		}
	}
}

FlickrSlideShow.prototype.preload = function( images ) {
	if( images == undefined ) images = this.images;

	for( var i = 0; i < images.length; i++ )
	{
		var img = new Image( );
		img.src = images[ i ];
	}	
}
