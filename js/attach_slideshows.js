$(document).ready( function() {
		var elements = $('.slideshow_image');

		$.each( elements, function(e) {
			$(this).flickrSlideshow( this.id, {
				sleep:      8,
				fade:       1.4,
				image_size: 3
			});
		});
	}
);
