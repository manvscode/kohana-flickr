<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
	'api_key' => 'API KEY GOES HERE',
	'api_secret' => 'API SECRET GOES HERE',
	'cache_file' => APPPATH . 'cache/flickr.cache',
	'config_file' => APPPATH . 'config/flickr.cfg',
);
