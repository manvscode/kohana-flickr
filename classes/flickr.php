<?php defined('SYSPATH') OR die('No direct access allowed.');

class Flickr
{
	protected static $api;

	public static function delete_cache( )
	{
		$cache = Cache::instance( );
		$cache->delete_all( );

		$config = Kohana::$config->load( 'flickr' );

		$cache_file = $config->get('cache_file');
		if( file_exists($cache_file) )
		{
			unlink( $cache_file );	
		}
	}


	////////////////////////////////////////////////////////////
	/////////// RAW METHODS: These use Phlickr_Cache ///////////
	////////////////////////////////////////////////////////////
	public static function photoset( $photoset_id )
	{
		self::initialize( );
		$photoset = new Phlickr_Photoset( self::$api, $photoset_id );
		return $photoset;
	}

	public static function photosets( )
	{
		self::initialize( );

		$list   = new Phlickr_PhotosetList( self::$api );
		$result = array();

		foreach( $list->getPhotosets() as $photoset )
		{
			array_push( $result, $photoset );
		}

		return $result;
	}

	public static function photos( $photoset_id )
	{
		self::initialize( );

		$photoset = new Phlickr_Photoset( self::$api, $photoset_id );
		$result   = array();
		
		foreach( $photoset->getPhotoList()->getPhotos() as $photo )
		{
			array_push( $result, $photo );
		}
			
		return $result;
	}

	public static function photo( $photo_id )
	{
		self::initialize( );
		$photo = new Phlickr_Photo( self::$api, $photo_id );
		return $photo;
	}

	protected static function initialize( )
	{
		$config    = Kohana::$config->load( 'flickr' );
		self::$api = Phlickr_Api::createFrom( $config->get('config_file') );
	}



	public static function activate( )
	{
		$config  = Kohana::$config->load('flickr');
		$api     = new Phlickr_Api( $config->get('api_key'), $config->get('api_secret') );

		// Authentication is no longer done with an email/password. the first step
		// is requesting a frob and then building a Flickr URL to log into.
		$frob = $api->requestFrob();

		$session = Session::instance( );
		$session->set( 'app_frob', $frob );

		// request read and write permissions (write implies read)
		$url = $api->buildAuthUrl( 'read', $frob );

		echo "<p> Go to this URL, to activate it. </p>";
		echo "<b>url:</b> $url<br />\n";
		echo "<p> Or, " . html::anchor( $url, "click here" ) . "</p>";
	}

	public static function generate_config( )
	{
		$config  = Kohana::$config->load('flickr');
		$api     = new Phlickr_Api( $config->get('api_key'), $config->get('api_secret') );
		$session = Session::instance( );
		$frob    = $session->get( 'app_frob', NULL ); 
		echo "$frob";

		// After they've granted permission, convert the frob to a token.
		$token = $api->setAuthTokenFromFrob( $frob );

		$api->setCacheFilename( $config->get('cache_file') );
		$api->saveAs( $config->get('config_file') );
	}

	public static function test( )
	{
		self::initialize( );

		// check if the authentication token is valid.
		if( self::$api->isAuthValid() )
		{
			// execute a flickr echo test method.
			$response = self::$api->executeMethod('flickr.test.echo', array('foo'=>'bar'));

			header( 'Content-Type: text/xml' );
			print $response;
		}
		else
		{
			print "Invalid authentication.";
		}
	}

	public static function auth_test( )
	{
		self::initialize( );

		// check if the authentication token is valid.
		if( self::$api->isAuthValid() )
		{
			// execute a flickr echo test method.
			$response = self::$api->executeMethod('flickr.test.login', array('foo'=>'bar'));

			header( 'Content-Type: text/xml' );
			print $response;
		}
		else
		{
			print "Invalid authentication.";
		}
	}
}

