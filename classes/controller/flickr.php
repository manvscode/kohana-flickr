<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Flickr extends Controller 
{
	const CACHE_LIFETIME = 43200; // 12 hours

	public function __construct( Request $request )
	{
		parent::__construct( $request );
	}

	public function action_index( )
	{
		$this->action_demo( );
	}

	public function action_demo( $photoset_id = '72157622834367648' )
	{
		javascript::add( 'attach_slideshows.js' );
		javascript::add( 'jquery.cross-slide.js' );
		javascript::add( 'jquery.flickr-slideshow.js' );
		$content = View::factory( 'flickr_demo' );
		$content->bind('photoset_id', $photoset_id );
		$this->setContent( $content );
	}

	public function action_demo1( )
	{
		// Cached
		Flickr::delete_cache( );
		echo Kohana::debug( Flickr::photosets( ) );
	}

	public function action_demo2( )
	{
		// Cached
		echo Kohana::debug( Flickr::photosets( ) );
	}

	public function action_demo3( )
	{
		// Non-cached
		echo Kohana::debug( Flickr::photosets( ) );
	}

	////////////////////////////////////////////////////////////
	///////////////////// CACHED METHODS ///////////////////////
	////////////////////////////////////////////////////////////
	public function action_ajax_photoset( $photoset_id, $lifetime = self::CACHE_LIFETIME ) // JSON Interfaces
	{
		$cache     = Cache::instance( 'default' );
		$cache_key = "ajax_photoset({$photoset_id})";
		$result    = $cache->get( $cache_key );

		if( $result === NULL )
		{
			$photoset = Flickr::photoset( $photoset_id );
			$photoset_data   = array(
				'id'          => $photoset->getId(),
				'title'       => $photoset->getTitle(),
				'description' => $photoset->getDescription(),
				'count'       => $photoset->getPhotoCount(),
				'url'         => $photoset->buildUrl(),
				'thumbnail'   => $photoset->buildThumbnailUrl(),
			);
			$result = json_encode( $photoset_data );
			$cache->set( $cache_key, $result, $lifetime );
		}

		$this->request->headers[ 'Content-Type' ] = 'application/json';
		echo $result; // JSON
	}

	public function action_ajax_photosets( $lifetime = self::CACHE_LIFETIME ) // JSON Interfaces
	{
		$cache     = Cache::instance( 'default' );
		$cache_key = 'ajax_photosets()';
		$result    = $cache->get( $cache_key );

		if( $result === NULL )
		{
			$photosets = Flickr::photosets( );

			$photosets_data = array();
			foreach( $photosets as $photoset )
			{
				array_push( $photosets_data, array( 
					'id'          => $photoset->getId(),
					'title'       => $photoset->getTitle(),
					'description' => $photoset->getDescription(),
					'count'       => $photoset->getPhotoCount(),
					'url'         => $photoset->buildUrl(),
				));
			}

			$result = json_encode( $photosets_data );
			$cache->set( $cache_key, $result, $lifetime );
		}

		$this->request->headers[ 'Content-Type' ] = 'application/json';
		echo $result; // JSON
	}

	public function action_ajax_photos( $photoset_id, $lifetime = self::CACHE_LIFETIME ) // JSON Interfaces
	{
		$cache     = Cache::instance( 'default' );
		$cache_key = "ajax_photos({$photoset_id})";
		$result    = $cache->get( $cache_key );

		if( $result === NULL )
		{
			$photos = Flickr::photos( $photoset_id );
			$result = array();

			foreach( $photos as $photo )
			{
				$photo_data = array(
					'id'          => $photo->getId(),
					'title'       => $photo->getTitle(),
					'description' => $photo->getDescription(),
					'image'       => $photo->buildImgUrl(),
					'url'         => $photo->buildUrl(),
					'sizes'       => $photo->getSizes(),
					'posted_ts'   => $photo->getPostedTimestamp(),
					'media'       => $photo->getMedia(),
					
				);

				// Handle special casing for videos	
				if( $photo->getMedia() == 'video' )
				{
					$photo_data[ 'video_width' ]      = $photo->getVideoWidth( );
					$photo_data[ 'video_height' ]     = $photo->getVideoHeight( );
					$photo_data[ 'video_url' ]        = $photo->buildVideoUrl( );
					$photo_data[ 'video_embed_code' ] = $photo->getVideoEmbedCode( );
				}

				array_push( $result, $photo_data );
			}

			$result = json_encode( $result );
			$cache->set( $cache_key, $result, $lifetime );
		}

		$this->request->headers[ 'Content-Type' ] = 'application/json';
		echo $result; // JSON
	}

	public function action_ajax_photo( $photo_id, $lifetime = self::CACHE_LIFETIME ) // JSON Interfaces
	{
		$cache     = Cache::instance( 'default' );
		$cache_key = "ajax_photo({$photo_id})";
		$result    = $cache->get( $cache_key );

		if( $result === NULL )
		{
			$photo      = Flickr::photo( $photo_id );
			$photo_data = array(
				'id'          => $photo->getId(),
				'title'       => $photo->getTitle(),
				'description' => $photo->getDescription(),
				'image'       => $photo->buildImgUrl(),
				'url'         => $photo->buildUrl(),
				'sizes'       => $photo->getSizes(),
				'posted_ts'   => $photo->getPostedTimestamp(),
				'media'       => $photo->getMedia(),
			);
			$result = json_encode( $photo_data );
			$cache->set( $cache_key, $result, $lifetime );
		}

		$this->request->headers[ 'Content-Type' ] = 'application/json';
		echo $result; // JSON
	}

	public function action_activate( )
	{
		Flickr::activate( );
	}

	public function action_generate_config( )
	{
		Flickr::generate_config( );
	}

	public function action_test( )
	{
		Flickr::test( );
	}
}
